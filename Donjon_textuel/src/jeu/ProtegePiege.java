package jeu;

public class ProtegePiege extends Objet{
	private int pointProtection;
	private static final String NOM_DEF="Protection contre les pi�ges";
	
	public ProtegePiege(String nom, int pointProtection) {
		super(nom, NOM_DEF);
		this.pointProtection=pointProtection;
	}

	public int getPointProtection() {
		return pointProtection;
	}

	public void setPointProtection(int pointProtection) {
		this.pointProtection = pointProtection;
	}
	
	public String toString() {
		return getNom()+" vous prot�ge a hauteur de "+getPointProtection()+" PV !";
	}
}
