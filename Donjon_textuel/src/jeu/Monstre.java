package jeu;

public class Monstre {
	protected double vie;
	protected String nom;
	protected int attaque;

	// Constructeur du Monstre avec le nom,vie et attaque

	public Monstre(String nom, int vie, int attaque) {
		super();
		this.nom = nom;
		this.vie = vie;
		this.attaque = attaque;
	}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public String toString() {
		return "\t -" + nom + " : VIE [" + vie + "] ATTAQUE [" + attaque
				+ "]";
	}

	public double getVie() {
		return vie;
	}

	public void setVie(double d) {
		this.vie = d;
	}

	public int getAttaque() {
		return attaque;
	}

	public void setAttaque(int attaque) {
		this.attaque = attaque;
	}

	// Fonction permettant au Monstre d'attaquer le Joueur

	public void attaque(Joueur J1) {
		try {
			if (this.getVie() > 0) {
				// Mise en Place du Système de Coup Critique
				if (Math.random() * 100 < 15) {
					// Si le monstre donne un coup critique, un message personalisé
					// apparait
					J1.setVie(J1.getVie() - (this.getAttaque()) * 1.5);
					Thread.sleep(1000);

					System.out.println("\tLe " + this.getNom()
							+ " s'�nerve et vous donne un �norme coup et vous enleve [" + attaque * 1.5 + "] PV");
				} else {
					// Sinon le monstre attaque normalement
					J1.setVie(J1.getVie() - this.getAttaque());

					Thread.sleep(1000);

					System.out.println(
							"\tLe " + this.getNom() + " r�plique et vous enleve un total de [" + attaque + "] PV");
				}
				// On affiche la vie de Joueur a la fin de chaque attaque du monstre
				Thread.sleep(1000);
				System.out.println("\n -VOTRE VIE [" + J1.getVie() + "]");
			}

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
	}

}
