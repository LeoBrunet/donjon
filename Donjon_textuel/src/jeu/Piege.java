package jeu;

public class Piege {
	private String nom;
	private int attaque;
	
	public Piege(String nom, int attaque) {
		this.setNom(nom);
		this.setAttaque(attaque);
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getAttaque() {
		return attaque;
	}

	public void setAttaque(int attaque) {
		this.attaque = attaque;
	}
	
	public void piegerJoueur(Joueur joueur) {
		ProtegePiege protection = joueur.getProtegePiege();
		
		if(protection==null) {
		joueur.setVie(joueur.getVie()-this.getAttaque());
		System.out.println("Le "+getNom()+" vous a infliger une attaque qui vous � retirer "+getAttaque()+" point de vie.");
		}
		else {
			if(protection.getPointProtection()>=this.getAttaque()) {
				System.out.println("Bien jou� ! Votre protection contre les pi�ges a encaiss� les d�gats inflig� par le pi�ge.");
			}
			else {
				joueur.setVie(joueur.getVie()-(this.getAttaque()-protection.getPointProtection()));
				System.out.println("Le "+getNom()+" vous a infliger une attaque qui vous � retirer "+getAttaque()+" point de vie. Heuresement que votre protection � encaisser "+protection.getPointProtection()+" d�gats!");
			}
			joueur.setVie(joueur.getVie());
			System.out.println("");
		}
	}
	
	public String toString() {
		return getNom() +" qui attaque � "+getAttaque();
	}
}
