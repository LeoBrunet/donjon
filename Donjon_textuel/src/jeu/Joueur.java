package jeu;

import java.util.ArrayList;
import java.util.Scanner;

public class Joueur {

	private ProtegePiege protegePiege = null;
	private Salle position = Donjon.getSalledebut();
	private String nom;
	private double vie;
	private int atk;
	private ItemOf arme;
	private Salle sallePrec = Donjon.getSalledebut();
	private final static int VIE_DEF = 200;
	private final static ItemOf ARME_DEF = new ItemOf("Stylo BIC 4 couleurs", 10000);

	// CREATION de la liste INVENTAIRE du Joueur
	public ArrayList<Objet> inventaire = new ArrayList<>();

	// CREATION du Joueur avec une vie et une attaque de base
	public Joueur(String nom) {
		this.setNom(nom);
		vie = VIE_DEF;
		arme = ARME_DEF;
		inventaire.add(ARME_DEF);
		inventaire.add(new ItemOf("Epee en bois", 10));
	}

	// Fonction pour l'INVENTAIRE

	public ArrayList<Objet> getInventaire() {
		return inventaire;
	}

	public void setInventaire(ArrayList<Objet> inventaire) {
		this.inventaire = inventaire;
	}

	public void afficheInventaire() {
		if (inventaire.size() == 0) {
			System.out.println("Votre inventaire est vide ! ");
		} else {
			System.out.println("\tVotre inventaire contient : ");
			for (int i = 0; i < inventaire.size(); i++) {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				System.out.println("\t -" + inventaire.get(i).getNom());
			}
		}

	}

	public void choixProtegePiege() {
		@SuppressWarnings("resource")
		Scanner s = new Scanner(System.in);
		System.out.println("Vos protections contre les pi�ges sont : ");
		ArrayList<ProtegePiege> inventaireP = new ArrayList<ProtegePiege>();
		for (int i = 0; i < inventaire.size(); i++) {
			if (inventaire.get(i) instanceof ProtegePiege) {
				inventaireP.add((ProtegePiege) inventaire.get(i));
			}
		}
		for (int i = 0; i < inventaireP.size(); i++) {
			System.out.println("\t -" + inventaireP.get(i).getNom() + " qui vous prot�ge jusqu'� :"
					+ inventaireP.get(i).getPointProtection() + " PV : (" + (i + 1) + ")");
		}
		System.out.println("Quel protection voulez vous �quiper? (0 si vous ne voulez pas �quiper de protection)");
		int choix = s.nextInt();
		while (choix > inventaireP.size() || choix < 0) {
			this.erreur();
			choix = s.nextInt();
		}
		if (choix == 0) {
			System.out.println("Vous avez d�cid� de ne pas �quipez de protection !");
			return;
		}
		setProtegePiege(inventaireP.get(choix - 1));
		System.out.println("Vous avez �quiper " + this.getProtegePiege().getNom() + " !");
	}

	private void erreur() {
		System.out.println("Veuillez rentrer un choix valide !");
	}

	public void choixArme() {
		@SuppressWarnings("resource")
		Scanner s = new Scanner(System.in);
		System.out.println("Vos armes sont : ");
		ArrayList<ItemOf> inventaireOf = new ArrayList<ItemOf>();
		for (int i = 0; i < inventaire.size(); i++) {
			if (inventaire.get(i) instanceof ItemOf) {
				inventaireOf.add((ItemOf) inventaire.get(i));
			}
		}
		for (int i = 0; i < inventaireOf.size(); i++) {
			System.out.println("\t -" + inventaireOf.get(i).getNom() + " qui vous octroie :"
					+ inventaireOf.get(i).getAttaque() + " points d'attaque : (" + (i + 1) + ")");
		}
		System.out.println("Quel arme voulez vous �quiper?");
		int choix = s.nextInt();
		while (choix > inventaireOf.size() || choix < 0) {
			this.erreur();
			choix = s.nextInt();
		}
		setArme(inventaireOf.get(choix - 1));
		System.out.println("Votre nouvelle arme active est donc " + getArme().getNom());
	}

	public ItemOf getArme() {
		return arme;
	}

	public void setArme(ItemOf arme) {
		this.arme = arme;
	}

	public void ouvrirCofrre(Coffre coffre) {
		inventaire.addAll(coffre.getCoffreList());
	}

	// GETTER / SETTER pour la vie & l'attaque du Joueur

	public void setVie(double d) { // pour set les points de vie
		this.vie = d;
	}

	public double getVie() { // pour avoir les points de vie
		return vie;
	}

	public int getAtk() { // pour connaitre l'ATK d'un joueur
		return atk;
	}

	public void setAtk(int atk) { // pour set les degat d'atk
		this.atk = atk;
	}

	// Fonction pour que le joueur attaque un Monstre
	public void attaque(Monstre MonstreM) {

		try {
			if (vie > 0) {
				Thread.sleep(1000);
				// Mise en Place du syst�me de coup Critique a 20%
				if (Math.random() * 100 < 20) {
					// Si le Joueur Donne un coup critique, on affiche un message
					// personalis�
					MonstreM.setVie(MonstreM.getVie() - getArme().getAttaque() * 1.5);
					Thread.sleep(800);

					System.out.println("\tVous DEBORDEZ d'�nergie et porter un puissant coup au " + MonstreM.getNom()
							+ " d'un total de [" + getArme().getAttaque() * 1.5 + "] de d�gats");
					Thread.sleep(1000);
					if (MonstreM.getVie() > 0) {
						System.out.println("\n -VIE DU " + MonstreM.getNom() + " [" + MonstreM.getVie() + "]");
					} else {
						MonstreM.setVie(0);
						System.out.println("\n -VIE DU " + MonstreM.getNom() + " [" + 0 + "]");
						Thread.sleep(1000);
					}

				} else {
					// Sinon le Joueur attaque normalement
					MonstreM.setVie(MonstreM.getVie() - getArme().getAttaque());
					Thread.sleep(700);
					System.out.println("\tVous passez � l'offensive et lui porter un coup d'un total de ["
							+ getArme().getAttaque() + "] de d�gats");
					Thread.sleep(1000);
					if (MonstreM.getVie() > 0) {
						System.out.println("\n -VIE DU " + MonstreM.getNom() + " [" + MonstreM.getVie() + "]");
					} else {
						MonstreM.setVie(0);
						Thread.sleep(1000);
						System.out.println("\n -VIE DU " + MonstreM.getNom() + " [" + 0 + "]");
					}
				}
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Fonction d'affichage de la vie du Joueur

	public void afficheInfo() {
		try {
			Thread.sleep(200);
			afficheVie();
			Thread.sleep(200);
			System.out.println("\t -VOTRE ARME ACTIVE [" + arme.getNom() + "]");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void afficheVie() {
		System.out.println("\t -VOTRE VIE [" + vie + "]");
	}

	public Salle choix(Salle positionJoueur) throws InterruptedException {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		@SuppressWarnings("resource")
		Scanner sc1 = new Scanner(System.in);

		String choix = "";
		String choixM = "";

		while (!(choix.equals("N") || (choix.equals("S") || choix.equals("O") || choix.equals("E")))) {

			// LECTURE DE L'ENTREE
			choix = sc.nextLine();

			// COMBAT
			if (choix.equals("X")) {
				examinerSalle(positionJoueur);

				if ((positionJoueur.hasMonstre())) {
					while (!(choixM.equals("A") || choixM.equals("F"))) {
						System.out.println("\n Dans la salle il y a un " + positionJoueur.getMonstre().toString());
						System.out.println("\n Vous pr�f�rez :" + "\n\t -Attaquer : (A)" + "\n\t -Fuir : (F)");
						choixM = sc1.nextLine();
						if (choixM.equals("A")) {

							try {
								Thread.sleep(1000);

								System.out.println("\n\tVous attaquez le " + positionJoueur.getMonstre().getNom()
										+ " avec votre " + arme.getNom() + "\n");
								Thread.sleep(200);

								while (getVie() > 0 && positionJoueur.getMonstre().getVie() > 0) {
									attaque(positionJoueur.getMonstre());
									if (positionJoueur.getMonstre().getVie() > 0)
										positionJoueur.getMonstre().attaque(this);
								}

								if (positionJoueur.getMonstre().getVie() <= 0) {
									Thread.sleep(200);
									System.out.println("\n[ BRAVO VOUS AVEZ REMPORTE VOTRE COMBAT CONTRE "
											+ positionJoueur.getMonstre().getNom() + " ]\n");
									positionJoueur.setMonstreMort();
									Thread.sleep(200);
									if (positionJoueur.hasCoffre()) {
										positionJoueur.getCoffre().afficheCoffre();
										recupererCoffre(positionJoueur.getCoffre());
									}
									positionJoueur.afficheMenu(this);
								} else {
									System.out.println("VOUS ETES MORT !");
									System.exit(1);
								}
								/* affichage.afficheMenu(this, positionJoueur); */
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} else if (choixM.equals("F")) {
							if (Math.random() < 0.25) {
								positionJoueur.getMonstre().attaque(this);
							} else {
								System.out.println("Vous avez de la chance, ce " + positionJoueur.getMonstre().getNom()
										+ " vous a rat� de peu!");
							}
							return getSallePrec();

						}

					}
				} else if (positionJoueur.hasCoffre()) {
					positionJoueur.getCoffre().afficheCoffre();
					recupererCoffre(positionJoueur.getCoffre());
				}

			}

			else if (choix.equals("C")) {
				this.choixArme();
			}

			// SE SOIGNER
			else if (choix.equals("H")) {
				this.soin();
			}

			// VIE
			else if (choix.equals("I")) {
				this.afficheInfo();
				this.afficheInventaire();
			}

			else if (choix.equals("Q")) {
				@SuppressWarnings("resource")
				Scanner s = new Scanner(System.in);
				System.out.println("Vous d�cidez donc de nous quittez ???? (oui/non)");
				String rep = s.nextLine();
				if (rep.equals("oui")) {
					System.out.println("Au revoir ch�re joueur.");
					System.out.println("");
					System.out.println("Vous avez quitt� le jeu.");
					System.exit(0);
				}
				if (rep.equals("non")) {
					System.out.println("Ahhh �a me rassure, bon jeu!");
				}

			}

			// EQUIPER UNE PROTECTION
			else if (choix.equals("P")) {
				this.choixProtegePiege();
			}
			// CHANGEMENT DE PORTE
			else if (choix.equals("N") || choix.equals("S") || choix.equals("O") || choix.equals("E")) {
				this.setSallePrec(positionJoueur);
				if (choix.equals("N") && positionJoueur.porteNord != null) {
					return positionJoueur.porteNord.chgtSalle(positionJoueur);
				} else if (choix.equals("S") && positionJoueur.porteSud != null) {
					return positionJoueur.porteSud.chgtSalle(positionJoueur);
				} else if (choix.equals("O") && positionJoueur.porteOuest != null) {
					return positionJoueur.porteOuest.chgtSalle(positionJoueur);
				} else if (choix.equals("E") && positionJoueur.porteEst != null) {

					return positionJoueur.porteEst.chgtSalle(positionJoueur);
				}
			} else {
				this.erreur();
			}
		}
		return positionJoueur;
	}

	private void recupererCoffre(Coffre coffre) {
		if (coffre.getCoffreList().size() > 0) {
			inventaire.addAll(coffre.getCoffreList());
			System.out.println("Vous avez ajoutez le contenue de ce coffrre dans votre inventaire !");
			coffre.setActif(false);
		}
	}

	// Fonction pour soigner le Joueur

	public void soin() {
		try {
			ItemUt objToRemove = null;
			// On parcours notre Inventaire
			for (Objet obj : inventaire) {
				// Si l'item n'est pas une ItemUt alors on continue de parcourir
				// notre inventaire
				if (!(obj instanceof ItemUt))
					continue;
				// L'objet utilitaire qu'on doit consommer est stock� dans une
				// variable tampon
				objToRemove = (ItemUt) obj;
				break;
			}
			// Si cette objet n'est pas nul
			if (objToRemove != null) {
				// On soigne le joueur en ne d�passant pas la vie max limite
				if (getVie() + objToRemove.getViee() < Joueur.VIE_DEF + objToRemove.getViee()) {
					inventaire.remove(objToRemove);
					setVie(getVie() + objToRemove.getViee());
					if (getVie() > Joueur.VIE_DEF) {
						setVie(Joueur.VIE_DEF);
					}
					System.out.println("\n\tVous consommer votre [" + objToRemove.getNom() + "]");
					System.out.println("\n\tVous vous soigner de [" + objToRemove.getViee() + "] HP\n");
					Thread.sleep(1000);
				} else {
					// Sinon on explique car le joueur est d�ja au maximum de sa
					// vie d�finie
					Thread.sleep(1000);
					System.out.println("\t\nVous ne pouvez pas vous soignez car vous �tes au MAX de votre VIE");
					Thread.sleep(1000);
				}

			} else {
				// Si aucun ItemUt est trouv� dans l'inventaire, un message est
				// envoy�
				Thread.sleep(300);
				System.out.println("\n\tVous n'avez pas de quoi vous soigner !");
				Thread.sleep(1000);
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// TODO Auto-generated method stub;

	}

	public String seDeplacer() {
		Scanner sc = new Scanner(System.in);
		String direction = sc.next();
		sc.close();
		return direction;
	}

	public void examinerSalle(Salle positionJoueur) {
		if (positionJoueur.hasCoffre() || positionJoueur.hasMonstre()) {

		}
		if (positionJoueur.hasCoffre() && positionJoueur.hasMonstre()) {
			System.out.println("Il y a un coffre et un monstre dans cette salle, pour r�cup�rer le "
					+ positionJoueur.getCoffre().getNom() + " il faudra battre " + positionJoueur.getMonstre().getNom()
					+ " !");
		} else if (positionJoueur.hasMonstre()) {
			System.out.println("");
		} else if (positionJoueur.hasCoffre()) {
			System.out.println("Il y as un " + positionJoueur.getCoffre().getNom() + " dans cette salle !");
		} else {
			System.out.println("Il n'y as rien de particulier dans cette salle !");
		}
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public boolean verifSoin() {
		int cpt = 0;
		for (Objet obj : inventaire) {
			if (obj instanceof ItemUt) {
				cpt++;
			}
		}
		if (cpt == 0) {
			return false;
		}
		return true;
	}

	public boolean verifArme() {
		int cpt = 0;
		for (Objet obj : inventaire) {
			if (obj instanceof ItemOf) {
				cpt++;
			}
		}
		if (cpt == 1) {
			return false;
		}
		return true;
	}

	public void setSallePrec(Salle sallePrec) {
		this.sallePrec = sallePrec;
	}

	public Salle getSallePrec() {
		return sallePrec;
	}

	public Salle getPosition() {
		return position;
	}

	public void setPosition(Salle salle) {
		position = salle;
	}

	public boolean verifProtectionPiege() {
		if (nombreDeProtection() == 0) {
			return false;
		}
		return true;
	}

	public int nombreDeProtection() {
		int nbProtection = 0;
		for (Objet obj : inventaire) {
			if (obj instanceof ProtegePiege) {
				nbProtection++;
			}
		}
		return nbProtection;
	}

	public ProtegePiege getProtegePiege() {
		return protegePiege;
	}

	public void setProtegePiege(ProtegePiege protegePiege) {
		this.protegePiege = protegePiege;
	}

}
