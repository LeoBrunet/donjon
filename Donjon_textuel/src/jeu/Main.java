package jeu;

public class Main {

	public static void main(String[] args) throws InterruptedException {
		Salle positionJoueur;
		Joueur J1 = new Joueur("John");
		Donjon d1 = new Donjon();
		d1.affichageRegle();
		positionJoueur = Donjon.getSalledebut();
		while (positionJoueur != d1.getSallefin()) {

			positionJoueur.afficheMenu(J1);

			// PIEGE
			if (positionJoueur.hasPiege()) {
				positionJoueur.getPiege().piegerJoueur(J1);
				J1.afficheVie();
			}

			positionJoueur = J1.choix(positionJoueur);

		}
		d1.fin();

	}

}
