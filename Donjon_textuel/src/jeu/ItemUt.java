package jeu;

public class ItemUt extends Objet {
	
	private static final String NOM_DEF="Item de soin";
	private int viee;

	// Constructeur des Item Utilitaires prenant en param�tre un nom et la vie
	// que l'item peut rendre
	public ItemUt(String nom, int viee) {
		super(nom, NOM_DEF);
		this.nom = nom;
		this.viee = viee;
	}

	public void setViee(int viee) {
		this.viee = viee;
	}

	public int getViee() {
		return viee;
	}
}
