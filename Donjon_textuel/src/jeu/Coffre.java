package jeu;

import java.util.ArrayList;

public class Coffre {

	private boolean actif = true;
	private String nom;
	// Creation de la liste d'un coffre
	private ArrayList<Objet> coffreList = new ArrayList<Objet>();

	// Constructeur du coffre prenant en param�tre un nom
	public Coffre(String nom) {
		this.setNom(nom);
		generationCoffre();

	}

	// Fonction permettant de g�n�rer le contenu du coffre dans la Salle
	public void generationCoffre() {
		double r = Math.random();
		if (r * 100 < 4) {
			ItemOf ObjetM = new ItemOf(" Ep�e en XML [45ATK]", 45);
			getCoffreList().add(ObjetM);

		}
		if (r * 100 < 2) {
			ItemOf ObjetM = new ItemOf(" GANTT DU CODE LEGENDAIRE [70ATK]", 70);
			getCoffreList().add(ObjetM);

		}
		if (r * 100 < 50) {
			ItemOf ObjetM = new ItemOf(" Couteau de survie[15ATK]", 15);
			getCoffreList().add(ObjetM);
		}

		if (r * 100 < 20) {
			ItemOf ObjetM = new ItemOf(" Lance de la Compta [20ATK]", 20);
			getCoffreList().add(ObjetM);

		}
		if (r * 100 < 12) {
			ItemOf ObjetM = new ItemOf(" Hache du RU [25ATK]", 25);
			getCoffreList().add(ObjetM);

		}
		if (r * 100 < 10) {
			ItemOf ObjetM = new ItemOf(" Carabine a IPV4 [30ATK]", 30);
			getCoffreList().add(ObjetM);
		}

		if (r * 100 < 6) {
			ItemOf ObjetM = new ItemOf(" Dagues Java [35ATK]", 35);
			getCoffreList().add(ObjetM);
		}

		if (r * 100 < 50) {
			ItemUt ObjetM = new ItemUt(" Salade du DENES [20HP]", 20);
			getCoffreList().add(ObjetM);

		}
		if (r * 100 < 15) {
			ItemUt ObjetM = new ItemUt(" Supreme K [40HP]", 40);
			getCoffreList().add(ObjetM);

		}

		if (r * 100 < 80) {
			ItemUt ObjetM = new ItemUt(" Caf� Expresso [10HP]", 10);
			getCoffreList().add(ObjetM);

		}
		if (r * 100 < 5) {
			ItemUt ObjetM = new ItemUt(" /iN//S/a//Ne /b//lo/oO/D// [100HP]", 100);
			getCoffreList().add(ObjetM);

		}
		if (r * 100 < 20) {
			ProtegePiege protegePiege = new ProtegePiege("un zinedine", 20);
			getCoffreList().add(protegePiege);
		}
		if (this.getNom() == "Coffre du d�but") {
			Objet protegePiege = new ProtegePiege("une protection", 20);
			getCoffreList().add(protegePiege);
		}
	}

	public void afficheCoffre() {
		if (getCoffreList().size() == 0) {
			System.out.println("\n\t Vous d�couvrer un " + getNom() + " qui est vide !");
		} else {
			System.out.println("\n\t Vous d�couvrer un " + getNom() + " qui contient : ");
			for (int j = 0; j < getCoffreList().size(); j++) {
				System.out.println("\t -" + getCoffreList().get(j).getNom());
			}
		}
	}

	public ArrayList<Objet> getCoffreList() {
		return coffreList;
	}

	public void setCoffreList(ArrayList<Objet> coffreList) {
		this.coffreList = coffreList;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public boolean isActif() {
		return actif;
	}

	public void setActif(boolean actif) {
		this.actif = actif;
	}

}
