package jeu;

public class Donjon {
	private static Salle salledebut;
	private Salle sallefin;

	public Donjon() {
		generation();

	}

	public static Salle getSalledebut() {
		return salledebut;
	}

	public Salle getSallefin() {
		return sallefin;
	}

	public void generation() {

		// instansciation des Salles
		salledebut = new Salle("Salle de d�but", null, null, new Piege("Piege de test", 20));
		Salle s1 = new Salle("[ SALLE DE L'AVENTURE ]",new Coffre("Coffre du d�but"), new Monstre("Cadavre d'info 2", 30, 10),null);
		Salle s2 = new Salle("[ SALLE ETRANGE ] ", new Coffre("Coffre Normal"), null, null);
		Salle s3 = new Salle("[ SALLE BANALE ]", null, null, null);
		Salle s4 = new Salle(" [ SALLE BD ]", null, null, null);
		Salle s5 = new Salle(" [ SALLE SM ]", null, null, null);
		Salle s6 = new Salle(" [ SALLE MALEFIQUE ]", null, null, null);
		Salle s7 = new Salle(" [ SALLE DU RANDOM ]", null, new Monstre(" BUG CAST JAVA ", 50, 20), null);
		Salle s8 = new Salle(" [ SALLE DU DUTE ]", new Coffre("Coffre de la ChAnCe"), null, null);
		Salle s9 = new Salle(" [ SALLE DU YOGA ]", null, new Monstre("Gob le Gobelin", 40, 15), null);
		Salle s10 = new Salle(" [ SALLE DE LA CAMOMILLE ]", new Coffre("Coffre du Vainqueur"), new Monstre("Gladiateur D�chu", 70, 15), null);
		Salle s11 = new Salle(" [ SALLE DU BEFORE ]", null, null, null);
		Salle s12 = new Salle(" [ SALLE SEDUISANTE ]", new Coffre("Coffre abandonn�"), null, null);
		Salle s13 = new Salle(" [ SALLE DE LA REORIENTATION ]", null, null, null);
		Salle s14 = new Salle(" [ SALLE DE LA MMI ]", new Coffre("/Co//Ff/Re B/u///G"), new Monstre(" D�fenseur de ParcoursSUP ", 100, 10), null);
		Salle s15 = new Salle(" [ SALLE DU REVE ]", null, null, null);
		Salle s16 = new Salle(" [ SALLE DISTORDU ]", null, null, null);
		Salle s17 = new Salle(" [ BUREAU DES BERMUDES ]", null, new Monstre(" GB irrit�e", 200, 5), null);
		Salle s18 = new Salle(" [ SALLE DE LA TRIFORCE ]", new Coffre("Le BON COFFRE "), null, null);
		Salle s19 = new Salle(" [ SALLE DU TROLL ]", null, new Monstre("El�ve fich� [S]", 80, 20), null);
		Salle s20 = new Salle(" [ SALLE DE MENAGE ]", null, null, null);
		Salle s21 = new Salle(" [ SALLE DU SECRETARIAT [9H/12H]", null, new Monstre("Requete SQL sauvage", 166, 16), null);
		Salle s22 = new Salle(" [ SALLE DU QUART D'HEURE MAYENNAIS ]", new Coffre("Lettre d'admission"), null, null);
		Salle s23 = new Salle(" [ TOILETTES ]", null, null, null);
		Salle s24 = new Salle(" [ BUREAU DU CHEF DEP ]", null, new Monstre("DUT", 300, 20), null);
		sallefin = new Salle("Salle finale", null, null, null);

		// On fixe les salles entre elles
		salledebut.fixeOuest(s1);
		s1.fixeSud(s2);
		s1.fixeNord(s3);
		s1.fixeOuest(s4);
		s4.fixeOuest(s5);
		s3.fixeNord(s6);
		s5.fixeNord(s7);
		s7.fixeNord(s8);
		s6.fixeOuest(s9);
		s9.fixeOuest(s8);
		s9.fixeNord(s10);
		s7.fixeOuest(s11);
		s11.fixeOuest(s12);
		s12.fixeSud(s13);
		s13.fixeSud(s14);
		s14.fixeOuest(s20);
		s14.fixeSud(s15);
		s15.fixeOuest(s19);
		s19.fixeNord(s20);
		s15.fixeEst(s16);
		s16.fixeSud(s17);
		s17.fixeEst(s18);
		s20.fixeOuest(s21);
		s21.fixeNord(s22);
		s22.fixeNord(s23);
		s23.fixeNord(s24);
		s24.fixeNord(sallefin);

		// On ajout les coffres et les monstres aux Salles

		
		/*s2.addCoffre("Coffre Normal");
		/s9.addMonstre("Gob le Gobelin", 40, 15);
		/s10.addCoffre("Coffre du Vainqueur");
		/s10.addMonstre("Gladiateur D�chu", 70, 15);
		/s8.addCoffre("Coffre de la ChAnCe");
		/s7.addMonstre(" BUG CAST JAVA ", 50, 20);
		/s12.addCoffre("Coffre abandonn�");
		/s14.addCoffre("/Co//Ff/Re B/u///G");
		/s14.addMonstre(" D�fenseur de ParcoursSUP ", 100, 10);
		/s19.addMonstre("El�ve fich� [S]", 80, 20);
		/s17.addMonstre(" GB irrit�e", 200, 5);
		/s18.addCoffre("Le BON COFFRE ");
		/s21.addMonstre("Requete SQL sauvage", 166, 16);
		/s22.addCoffre("Lettre d'admission");
		/s24.addMonstre("DUT", 300, 20);*/

	}

	public void fin() {
		System.out.println("\t BRAVO VOUS AVEZ ATTEINT LA SALLE FINALE VOUS ETES LIBRE ");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.exit(1);
	}
	
	public void affichageRegle() {
		System.out.println(" ,gggggggggggg,      _,gggggg,_      ,ggg, ,ggggggg,    gg       _,gggggg,_      ,ggg, ,ggggggg,          ,gggggggggggg,    ,ggg,         gg          ,gggggggggggg,    ,ggg,         gg ,ggggggggggggggg\r\n" + 
				"dP\"\"\"88\"\"\"\"\"\"Y8b,  ,d8P\"\"d8P\"Y8b,   dP\"\"Y8,8P\"\"\"\"\"Y8b  dP8,    ,d8P\"\"d8P\"Y8b,   dP\"\"Y8,8P\"\"\"\"\"Y8b        dP\"\"\"88\"\"\"\"\"\"Y8b, dP\"\"Y8a        88         dP\"\"\"88\"\"\"\"\"\"Y8b, dP\"\"Y8a        88dP\"\"\"\"\"\"88\"\"\"\"\"\"\"\r\n" + 
				"Yb,  88       `8b,,d8'   Y8   \"8b,dPYb, `8dP'     `88 dP Yb   ,d8'   Y8   \"8b,dPYb, `8dP'     `88        Yb,  88       `8b,Yb, `88        88         Yb,  88       `8b,Yb, `88        88Yb,_    88       \r\n" + 
				" `\"  88        `8bd8'    `Ybaaad88P' `\"  88'       88,8  `8,  d8'    `Ybaaad88P' `\"  88'       88         `\"  88        `8b `\"  88        88          `\"  88        `8b `\"  88        88 `\"\"    88       \r\n" + 
				"     88         Y88P       `\"\"\"\"Y8       88        88I8   Yb  8P       `\"\"\"\"Y8       88        88             88         Y8     88        88              88         Y8     88        88        88       \r\n" + 
				"     88         d88b            d8       88        88`8b, `8, 8b            d8       88        88             88         d8     88        88              88         d8     88        88        88       \r\n" + 
				"     88        ,8PY8,          ,8P       88        88 `\"Y88888Y8,          ,8P       88        88             88        ,8P     88        88              88        ,8P     88        88        88       \r\n" + 
				"     88       ,8P'`Y8,        ,8P'       88        88     \"Y8 `Y8,        ,8P'       88        88             88       ,8P'     88        88              88       ,8P'     88        88  gg,   88       \r\n" + 
				"     88______,dP'  `Y8b,,__,,d8P'        88        Y8,     ,88,`Y8b,,__,,d8P'        88        Y8,            88______,dP'      Y8b,____,d88,             88______,dP'      Y8b,____,d88,  \"Yb,,8P       \r\n" + 
				"    888888888P\"      `\"Y8888P\"'          88        `Y8 ,ad88888  `\"Y8888P\"'          88        `Y8           888888888P\"         \"Y888888P\"Y8            888888888P\"         \"Y888888P\"Y8    \"Y8P'       \r\n" + 
				"                                                     ,dP\"'   Yb                                                                                                                                          \r\n" + 
				"                                                    ,8'      I8                                                                                                                                          \r\n" + 
				"                                                   ,8'       I8                                                                                                                                          \r\n" + 
				"                                                   I8,      ,8'                                                                                                                                          \r\n" + 
				"                                                   `Y8,___,d8'                                                                                                                                           \r\n" + 
				"                                                     \"Y888P\"                                      ");
		System.out.println("\n*********************************************************************************************************************************************************************************************************");
		System.out.println(" 						 _      ______  _____   _____  ______ _____ _      ______  _____   _____  _    _        _ ______ _    _ \r\n" + 
				" 						| |    |  ____|/ ____| |  __ \\|  ____/ ____| |    |  ____|/ ____| |  __ \\| |  | |      | |  ____| |  | |\r\n" + 
				" 						| |    | |__  | (___   | |__) | |__ | |  __| |    | |__  | (___   | |  | | |  | |      | | |__  | |  | |\r\n" + 
				"						| |    |  __|  \\___ \\  |  _  /|  __|| | |_ | |    |  __|  \\___ \\  | |  | | |  | |  _   | |  __| | |  | |\r\n" + 
				" 						| |____| |____ ____) | | | \\ \\| |___| |__| | |____| |____ ____) | | |__| | |__| | | |__| | |____| |__| |\r\n" + 
				"						|______|______|_____/  |_|  \\_\\______\\_____|______|______|_____/  |_____/ \\____/   \\____/|______|\\____/ ");
		System.out.println("\n*********************************************************************************************************************************************************************************************************");
		System.out.println("\nVous incarnez un personnage perdu dans un donjon, votre objectif est de trouv� la sortie en �vitant les monstres qui ne r�ves que de vous tuer.");
		System.out.println("Lorsque vous aurez entr� votre nom, le jeu commencera et vous pourrez voir s'afficher un menu. Celui ci vous motreras les possibilit�s d'action que vous avez dans cette salle.");
		System.out.println("\nPour r�aliser une action il faut taper la lettre placer � c�t� de celle ci.");
		System.out.println("\nPlusieurs choix s'offrieront � vous dont les changements de salle, il s'effectue avec les lettre 'S', 'N', 'O' et 'E' qui sont les premi�res lettres de chaques cardinalit�s!");
		System.out.println("On vous offre �galement la possibilit� d'afficher vos informations (point de vie restant, arme active, inventaire) en tapant la lettre 'I'.");
		System.out.println("Vous pourez �galement op�rez des changements sur votre inventaire en changeant votre arme active et ce en saisissant la lettre 'C'.");
		System.out.println("Enfin dans chaque salle, vous aurez la possibilit� d'examiner celle-ci, cette action est risqu�e car si vous l'�xecuter vous pourriez tomber sur un monstre,"
				+ " \ncependant vous pourriez �galement trouver un coffre, cet action pourra �tre r�alis� avec la lettre 'X'. ");
		
		
	}
}