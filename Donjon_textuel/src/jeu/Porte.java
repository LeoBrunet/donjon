package jeu;

public class Porte {
	Salle salle1;
	Salle salle2;
	//private String sens1vers2;
	//private String sens2vers1;

	// Constructeur d'une porte prenant en param�tre les 2 salles et leurs
	// cardinalit�s
	public Porte(Salle salle1, Salle salle2, String sens1vers2, String sens2vers1) {
		super();
		this.salle1 = salle1;
		this.salle2 = salle2;
		//this.setSens1vers2(sens1vers2);
		//this.setSens2vers1(sens2vers1);
	}

	// Fonction permettant de changer de salle qui s'adapte au cas ou pour
	// revenir en arri�re
	public Salle chgtSalle(Salle positionJoueur) {
		if (salle1 == positionJoueur) {
			return salle2;
		} else {
			return salle1;
		}

	}

	/*public String getSens1vers2() {
		return sens1vers2;
	}

	public void setSens1vers2(String sens1vers2) {
		this.sens1vers2 = sens1vers2;
	}

	public String getSens2vers1() {
		return sens2vers1;
	}

	public void setSens2vers1(String sens2vers1) {
		this.sens2vers1 = sens2vers1;
	}*/

}
