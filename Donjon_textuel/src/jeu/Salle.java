package jeu;

import java.util.ArrayList;

public class Salle {
	private static final String NOM_DEFAUT = "Salle du Donjon";
	private String nom;
	
	private Piege piege;
	private Coffre coffre;
	private Monstre monstre;
	// Salle � 4 porte pr�d�finie
	public Porte porteNord = null;
	public Porte porteSud = null;
	public Porte porteOuest = null;
	public Porte porteEst = null;
	public ArrayList<Porte> porteList = new ArrayList<Porte>();

	// Un salle � en pr�d�finition un monstre, un coffre
	/*public Monstre monstreM;
	public Coffre coffreM;
	protected String coffreNom;
	boolean monstreAdd;
	boolean coffreAdd;*/

	// Constructeur de Salle

	public Salle(String nom, Coffre coffre, Monstre monstre, Piege piege) {
		this.nom = nom;
		if (nom == "")
			nom = NOM_DEFAUT;
		this.monstre=monstre;
		this.coffre=coffre;
		this.piege=piege;
	}

	public void afficheMenu(Joueur J1) {
		System.out.println("\nVous �tes dans la " + this.getNom());
		System.out.println("\t -Quitter le jeu : (Q)");
		System.out.println("\t -Afficher vos informations : (I)");
		if (J1.verifSoin() == true) {
			System.out.println("\t -Se Soigner : (H)");
		}
		if (J1.verifArme() == true) {
			System.out.println("\t -Changer d'arme : (C)");
		}
		if (J1.verifProtectionPiege() == true) {
			System.out.println("\t -Equiper une protection : (P)");
		}
		System.out.println("\t -Examiner la salle : (X)");
		if (this.porteNord != null)
			System.out.println("\t -Entrer dans la salle au Nord : (N)");
		if (this.porteSud != null)
			System.out.println("\t -Entrer dans la salle au Sud : (S) ");
		if (this.porteOuest != null)
			System.out.println("\t -Entrer dans la salle � l'Ouest : (O)");
		if (this.porteEst != null)
			System.out.println("\t -Entrer dans la salle � l'Est : (E)");

	}
	
	public boolean hasPiege() {
		if(piege==null) {
			return false;
		}else {
			System.out.println("\nIl y a un pi�ge dans cette salle !");
		}
		return true;
	}
	
	public boolean hasMonstre() {
		if(monstre==null) {
			return false;
		}
		return true;
	}

	public boolean hasCoffre() {
		if(coffre==null) {
			return false;
		}
		return true;
	}
	// Fonction permettant de fixer une salle � une autre
	public void fixeNord(Salle salle) {

		Porte porte = new Porte(this, salle, "N", "S");
		porteNord = porte;
		salle.addPorteNord(porte);

	}

	public void fixeSud(Salle salle) {
		Porte porte = new Porte(this, salle, "Sud", "Nord");
		porteSud = porte;

		salle.addPorteSud(porte);

	}

	public void fixeOuest(Salle salle) {
		Porte porte = new Porte(this, salle, "Ouest", "Est");
		porteOuest = porte;
		salle.addPorteOuest(porte);

	}

	public void fixeEst(Salle salle) {
		Porte porte = new Porte(this, salle, "Est", "Ouest");
		porteEst = porte;
		salle.addPorteEst(porte);
	}
	// Ajoute une Porte � la salle

	private void addPorteNord(Porte porte) {
		// TODO Auto-generated method stub
		porteSud = porte;
		porteList.add(porteSud);
		

	}

	private void addPorteSud(Porte porte) {
		// TODO Auto-generated method stub
		porteNord = porte;
		porteList.add(porteNord);
	}

	private void addPorteOuest(Porte porte) {
		// TODO Auto-generated method stub
		porteEst = porte;
		porteList.add(porteEst);

	}

	private void addPorteEst(Porte porte) {
		// TODO Auto-generated method stub
		porteOuest = porte;
		porteList.add(porteOuest);

	}
	
	public Piege getPiege() {
		return piege;
	}

	public void setPiege(Piege piege) {
		this.piege = piege;
	}
	
	public void setMonstreMort() {
		monstre=null;
	}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Coffre getCoffre() {
		return coffre;
	}

	public void setCoffre(Coffre coffre) {
		this.coffre = coffre;
	}

	public Monstre getMonstre() {
		return monstre;
	}

	public void setMonstre(Monstre monstre) {
		this.monstre = monstre;
	}

	// Fonction permettant d'ajouter & afficher un Monstre et de savoir si celui
	// ci est pr�sent dans la salle
	/*public void addMonstre(String nom, int vie, int attaque) {
		monstreM = new Monstre(nom, vie, attaque);
		monstreAdd = true;
	}

	public String afficherMonstre() {
		return monstreM.toString();
	}

	public boolean hasMonstre() {
		return monstreAdd;
	}

	// Fonction permettant d'ajouter & afficher un coffre et de savoir si celui
	// ci est pr�sent dans la salle

	public void addCoffre(String nom3) {
		coffreM = new Coffre(nom);
		coffreAdd = true;
		coffreNom = nom3;

	}

	public boolean hasCoffre() {
		return coffreAdd;
	}

	public void getPorte() {
		
	}*/
	

	// Fonction de combat entre un monstre & un joueur ou les fonctions
	// "attaque" de joueur et de monstre sont appel�es

	/*public void combat(Joueur J1, Monstre MonstreM) {
		try {
			Thread.sleep(1000);

			System.out.println("\n\t[ VOUS ATTAQUEZ AVEC VOTRE ARME LA PLUS FORTE ] \n");
			Thread.sleep(2000);
			J1.nomArme = Joueur.NOM_ARME;
			System.out.println("\tVous attaquez le " + MonstreM.getNom() + " avec [" + J1.nomArme + "]\n");
			Thread.sleep(1000);

			while (J1.getVie() > 0 && MonstreM.getVie() > 0) {
				J1.attaque(MonstreM);
				if (MonstreM.getVie() > 0)
					MonstreM.attaque(J1);
			}

			if (MonstreM.getVie() <= 0) {
				Thread.sleep(1000);
				System.out.println("\n[ BRAVO VOUS AVEZ REMPORTE VOTRE COMBAT CONTRE " + MonstreM.getNom() + " ]\n");
				this.monstreAdd = false;
				Thread.sleep(2000);
			} else {
				System.out.println("VOUS ETES MORT !");
				System.exit(1);
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/

}
