package jeu;

public class Objet {
	public String nom;

	// Classe Objet m�re des classes ItemOf & ItemUt
	public Objet(String nom, String NOM_DEF) {
		if(nom==null) {
			this.nom=NOM_DEF;
		}else {
			this.nom=nom;
		}
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

}
