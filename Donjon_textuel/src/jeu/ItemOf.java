package jeu;

public class ItemOf extends Objet {

	private static final String NOM_DEF="Item d'attaque";
	private int attaque;

	// Constructeur des Item Offensif prenant en param�tre un nom et une attaque
	public ItemOf(String nom, int attaque) {
		super(nom, NOM_DEF);
		this.attaque=attaque;
	}

	public int getAttaque() {
		return attaque;
	}

	public void setAttaque(int attaque) {
		this.attaque = attaque;
	}


}
